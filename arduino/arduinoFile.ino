#include <EmonLib.h>
#include "VL53L0X.h"
#include "Wire.h"

#define VOLT_CAL 655
#define VOLT_CAL2 425   
#define AMPERE_CAL 10//input
#define AMPERE_CAL1 10 //laser Calibragem OK - alicate elétrica
#define Yaddr 0x30

EnergyMonitor sensorsInput;
EnergyMonitor currentInput;
VL53L0X sensY;

int yNew = 0;

void lerSensores(){//função para ler os sensores
    // xNew = sensX.readRangeContinuousMillimeters()/10;
    Serial.println("Laser");
    yNew = sensY.readRangeSingleMillimeters();
    if (sensY.timeoutOccurred()) Serial.print(" TIMEOUT"); 
}

void setup(){
    //Serial with DragonBoard

    Serial.println("Setup");

    Serial.begin(38400);
    Wire.begin();
    
    currentInput.current(A0, AMPERE_CAL);     //sensor de corrente maquina
    sensorsInput.current(A1, AMPERE_CAL1);     //sensor de corrente laser 
    sensorsInput.voltage(A2, VOLT_CAL, 1.7);  //sensor de tensão do laser
    currentInput.voltage(A3, VOLT_CAL2, 1.7); //sensor de tensão motor/solenoide  
}

void loop(){
  Serial.println("Calc");
  sensorsInput.calcVI(20,2000);
  currentInput.calcVI(20,2000);
  float voltage = sensorsInput.Vrms;
  float voltage2 = currentInput.Vrms;
  float current1 = 3*sensorsInput.Irms;
  float current2 = 3*currentInput.Irms;
  //lerSensores();

  Serial.print("1010;");
  Serial.print(voltage);
  Serial.print(";");
  Serial.print(voltage2);
  Serial.print(";");
  Serial.print(current1);
  Serial.print(";");
  Serial.print(current2);
  Serial.print(";");
  Serial.print(yNew);
  Serial.println(";1011");
}
