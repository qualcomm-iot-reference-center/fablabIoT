import serial, csv, os, datetime, time
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

os.system("echo **********ABRINDO_SCRIPT**********")
#time.sleep(120)
os.system("echo **********INICIANDO_PROGRAMA**********")

ard = serial.Serial('/dev/tty96B0', 38400)
filename = '/home/linaro/fablabIoT/data/data.csv'
cred = credentials.Certificate("/home/linaro/fablabIoT/webapp/cred/firebase.json")
default_app = firebase_admin.initialize_app(cred, {'databaseURL': "https://fablabdragonboard.firebaseio.com/"})

while(1):

    ardOut = ard.readline()
    rawData = ardOut.decode().split(';')

    status = False
    if(rawData[0] == '1010' and rawData[6] == '1011\r\n'):
        status = True

    if(status):
        data = {'TimeStamp': str(datetime.datetime.now()),
                'Voltage_1': float(rawData[1]),
                'Voltage_2': float(rawData[2]),
                'Current_1': float(rawData[3]),
                'Current_2': float(rawData[4])
                }

        # if os.path.exists(filename):
        #     append_write = 'a'  # append if already exists
        # else:
        #     append_write = 'w'  # make a new file if not

        # with open(filename, append_write) as f:
        #     fieldnames = ['TimeStamp', 'Voltage_1', 'Voltage_2', 'Current_1', 'Current_2']
        #     writer = csv.DictWriter(f, fieldnames=fieldnames)
        #     if(append_write == 'w'):
        #         writer.writeheader()
        #     writer.writerow(data)

        root = db.reference()
        root.push(data)
