from flask import Flask, render_template, jsonify
import plotly.graph_objs as go
import json, plotly, firebase_admin
from firebase_admin import db

app = Flask(__name__)
cred = firebase_admin.credentials.Certificate('cred/firebase.json')
default_app = firebase_admin.initialize_app(cred, {'databaseURL': "https://fablabdragonboard.firebaseio.com/"})
ref = db.reference()

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/report')
def report():
    return render_template('report.html')

@app.route('/rt')
def rt_info():
    return render_template('real_time.html')

@app.route('/rt/_update', methods=['GET'])
def update():
    last_entry = list(ref.order_by_key().limit_to_last(1).get().items())[0][1]

    laser_power = round(last_entry['Current_1']*last_entry['Voltage_1'], 1)

    laser_status = 'off'
    if(last_entry['Current_2'] > 0.5):
        laser_status = 'on'

    machine_status = 'off'
    if(last_entry['Current_1'] > 1.0 or laser_status == 'on'):
        machine_status = 'on'

    return jsonify(
        x=last_entry['TimeStamp'], v1=last_entry['Voltage_1'],
        v2=last_entry['Voltage_2'], c1=last_entry['Current_1'],
        c2=last_entry['Current_2'], laser_power=laser_power,
        m_status=machine_status, l_status=laser_status
        )


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000)
